﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipProjectile : MonoBehaviour
{
    [SerializeField]
    private float speed;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        rb.velocity = new Vector2(0f, speed);
    }

    private void Update()
    {
        if (transform.position.y > GameManager.upperRight.y)
        {
            Destroy(gameObject);
        }
    }
}
