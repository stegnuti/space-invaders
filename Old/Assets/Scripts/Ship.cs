﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private int lives = 3;

    [SerializeField]
    private float fireCooldown = 0.2f;

    [SerializeField]
    private ShipProjectile projectilePrefab;

    private Rigidbody2D rb;

    private float halfShipWidth;
    private bool fireEnabled = true;
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

        halfShipWidth = GetComponent<SpriteRenderer>().bounds.extents.x;
    }

    private float axis;
    private void Update()
    {
        axis = Input.GetAxisRaw("Horizontal");

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (fireEnabled)
            {
                Fire();
            }
        }
    }

    private void Fire()
    {
        // Fire projectile and put firing on cooldown
        Instantiate(projectilePrefab, transform.position, Quaternion.identity);

        fireEnabled = false;
        StartCoroutine(_FireCooldown(fireCooldown));
    }

    private IEnumerator _FireCooldown(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        fireEnabled = true;
    }

    private float newPos;
    private void FixedUpdate()
    {
        // Calculate move speed
        rb.velocity = new Vector2(moveSpeed * axis, 0);

        // Prevent going off screen
        newPos = transform.position.x + axis * halfShipWidth + rb.velocity.x * Time.deltaTime;
        if (newPos > GameManager.upperRight.x || newPos < GameManager.lowerLeft.x)
        {
            rb.velocity = Vector2.zero;
        }
    }
}
