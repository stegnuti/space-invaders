using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static InputState state;

    public static bool Fire => state.Fire;
    public static float MoveHorizontal => state.MoveHorizontal;
    public static float MoveVertical => state.MoveVertical;

    private readonly string FireKey = "Fire";
    private readonly string HorizontalMoveKey = "Horizontal";
    private readonly string VerticalMoveKey = "Vertical";

    private void Update()
    {
        state.ResetWasModified();
        state.Fire = Input.GetButtonDown(FireKey);
        state.MoveHorizontal = Input.GetAxisRaw(HorizontalMoveKey);
        state.MoveVertical = Input.GetAxisRaw(VerticalMoveKey);
    }
}
