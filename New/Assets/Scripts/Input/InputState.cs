﻿using System;

public struct InputState
{
    private static readonly float AxisChangeTolerance = 0.001f;
    
    private bool wasModified;
    
    private bool fire;
    private float moveHorizontal;
    private float moveVertical;

    public float MoveHorizontal
    {
        get => moveHorizontal;
        set
        {
            if (Math.Abs(moveHorizontal - value) < AxisChangeTolerance) return;
            moveHorizontal = value;
            wasModified = true;
        }
    }
    
    public float MoveVertical
    {
        get => moveVertical;
        set
        {
            if (Math.Abs(moveVertical - value) < AxisChangeTolerance) return;
            moveVertical = value;
            wasModified = true;
        }
    }

    public bool Fire
    {
        get => fire;
        set
        {
            if (fire == value) return;
            fire = value;
            wasModified = true;
        }
    }

    public bool WasModified => wasModified;
    
    public void ResetWasModified()
    {
        wasModified = false;
    }
}